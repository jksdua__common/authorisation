/* globals describe, beforeEach, before, it */

'use strict';

describe('#authorisation', function() {
	var chai = require('chai');
	var expect = chai.expect;

	var auth = require(__dirname + '/../src')();

	var ID1, ID2, ID3, ID4, ID5, ID6;

	before(function *() {
		ID1 = yield auth._id();
		ID2 = yield auth._id();
		ID3 = yield auth._id();
		ID4 = yield auth._id();
		ID5 = yield auth._id();
		ID6 = yield auth._id();
	});

	beforeEach(function* () {
		yield auth._db.flushdb();
	});

	function eqlRule(actual, expected) {
		// ensure keys match, otherwise dont bother
		expect(Object.keys(actual).sort()).to.eql(Object.keys(expected).sort());

		for (var i in actual) {
			if ('created_at' !== i) {
				expect(actual[i]).to.eql(expected[i]);
			}
		}
	}

	describe('#constants', function() {
		it('should exist', function() {
			expect(auth.constants).to.be.an('object');
		});
	});

	describe('#parent', function() {
		describe('#add', function() {
			it('should add a parent', function* () {
				yield auth.parent.add(ID1, ID2);
				var parents = yield auth.parent.get(ID1);
				expect(parents).to.eql([ID2]);
			});

			it('should not add an aready existing parent', function* () {
				yield [
					auth.parent.add(ID1, ID2),
					auth.parent.add(ID1, ID3)
				];

				// duplicate parent
				yield auth.parent.add(ID1, ID2);

				var parents = yield auth.parent.get(ID1);
				expect(parents).to.have.length(2);
				expect(parents).to.contain(ID2);
				expect(parents).to.contain(ID3);
			});
		});

		describe('#get', function() {
			it('should return an empty array if no parents exist', function* () {
				expect(yield auth.parent.get(ID1)).to.eql([]);
			});

			it('should get all the parents of a resource', function* () {
				yield [
					auth.parent.add(ID1, ID2),
					auth.parent.add(ID1, ID3)
				];

				var parents = yield auth.parent.get(ID1);
				expect(parents).to.have.length(2);
				expect(parents).to.contain(ID2);
				expect(parents).to.contain(ID3);
			});
		});

		describe('#del', function() {
			it('should remove a parent if it exists', function* () {
				// setup
				yield [
					auth.parent.add(ID1, ID2),
					auth.parent.add(ID1, ID3)
				];

				// do work
				yield auth.parent.del(ID1, ID3);

				var parents = yield auth.parent.get(ID1);
				expect(parents).to.eql([ID2]);
			});

			it('should silently return if parent does not exist', function* () {
				yield auth.parent.del(ID1, ID3);

				var parents = yield auth.parent.get(ID1);
				expect(parents).to.eql([]);
			});
		});
	});

	describe('#permission', function() {
		describe('#add', function() {
			describe('#static', function() {
				it('should fail if rule is missing resource', function* () {
					var called = false;
					try {
						yield auth.permission.add(ID1, {
							type: 'static',
							action: ['*']
						});
					} catch(e) {
						called = true;
					}
					expect(called).to.equal(true);
				});

				it('should fail if rule is missing action', function* () {
					var called = false;
					try {
						yield auth.permission.add(ID1, {
							type: 'static',
							resourcd: ID2
						});
					} catch(e) {
						called = true;
					}
					expect(called).to.equal(true);
				});

				it('should successfully add a rule and return it', function* () {
					var ruleId = yield auth.permission.add(ID1, {
						type: 'static',
						action: ['*'],
						resource: ID2
					});

					// ensure it exists for resource
					expect(yield auth.permission.get(ID1)).to.contain(ruleId);
					// ensure details are correct
					var retrievedRule = yield auth.permission.getDetails(ruleId);
					eqlRule(retrievedRule, {
						_id: ruleId,
						type: 'static',
						scope: ['*'],
						action: ['*'],
						status: 'enabled',
						access: 'allow',
						priority: 100,
						resource: ID2,
						description: '',
						dataSchema: null,
						created_at: Date.now()
					});
				});
			});

			describe('#script', function() {
				it('should fail if script is missing', function* () {
					var called = false;
					try {
						yield auth.permission.add(ID1, {
							type: 'script',
							action: 'bla',
							dataSchema: {}
						});
					} catch(e) {
						called = true;
					}
					expect(called).to.equal(true);
				});

				it('should fail if script is not a valid lua script', function* () {
					var called = false;
					try {
						yield auth.permission.add(ID1, {
							type: 'script',
							action: 'bla',
							script: 'abcd',
							dataSchema: {}
						});
					} catch(e) {
						called = true;
					}
					expect(called).to.equal(true);
				});

				it('should fail if dataSchema is missing', function* () {
					var called = false;
					try {
						yield auth.permission.add(ID1, {
							type: 'script',
							action: 'bla',
							script: 'return 10'
						});
					} catch(e) {
						called = true;
					}
					expect(called).to.equal(true);
				});

				it('should successfully add a rule', function* () {
					var ruleId = yield auth.permission.add(ID1, {
						type: 'script',
						action: ['*'],
						script: 'return 10',
						dataSchema: {}
					});

					// ensure it exists for resource
					expect(yield auth.permission.get(ID1)).to.contain(ruleId);
					// ensure details are correct
					var retrievedRule = yield auth.permission.getDetails(ruleId);
					eqlRule(retrievedRule, {
						_id: ruleId,
						type: 'script',
						scope: ['*'],
						action: ['*'],
						script: 'return 10',
						dataSchema: {},
						status: 'enabled',
						access: 'allow',
						priority: 100,
						description: '',
						created_at: Date.now()
					});
				});

				it('should successfully add a rule with a lua script of multiple lines', function* () {
					var ruleId = yield auth.permission.add(ID1, {
						type: 'script',
						action: ['*'],
						script: ['local a = 10', 'return a'].join('\n'),
						dataSchema: {}
					});

					// ensure it exists for resource
					expect(yield auth.permission.get(ID1)).to.contain(ruleId);
					// ensure details are correct
					var retrievedRule = yield auth.permission.getDetails(ruleId);
					eqlRule(retrievedRule, {
						_id: ruleId,
						type: 'script',
						scope: ['*'],
						action: ['*'],
						script: ['local a = 10', 'return a'].join('\n'),
						dataSchema: {},
						status: 'enabled',
						access: 'allow',
						priority: 100,
						description: '',
						created_at: Date.now()
					});
				});
			});

			describe('#general', function() {
				it('should allow adding permission in disabled state', function* () {
					var ruleId = yield auth.permission.add(ID1, {
						type: 'script',
						action: ['*'],
						script: 'return 10',
						status: 'disabled',
						dataSchema: {}
					});

					// ensure it exists for resource
					expect(yield auth.permission.get(ID1)).to.contain(ruleId);
					// ensure details are correct
					var retrievedRule = yield auth.permission.getDetails(ruleId);
					eqlRule(retrievedRule, {
						_id: ruleId,
						type: 'script',
						scope: ['*'],
						action: ['*'],
						script: 'return 10',
						dataSchema: {},
						status: 'disabled',
						access: 'allow',
						priority: 100,
						description: '',
						created_at: Date.now()
					});
				});

				it('should allow adding a deny rule', function* () {
					var ruleId = yield auth.permission.add(ID1, {
						type: 'script',
						action: ['*'],
						access: 'block',
						script: 'return 10',
						dataSchema: {}
					});

					// ensure it exists for resource
					expect(yield auth.permission.get(ID1)).to.contain(ruleId);
					// ensure details are correct
					var retrievedRule = yield auth.permission.getDetails(ruleId);
					eqlRule(retrievedRule, {
						_id: ruleId,
						type: 'script',
						scope: ['*'],
						action: ['*'],
						script: 'return 10',
						dataSchema: {},
						status: 'enabled',
						access: 'block',
						priority: 100,
						description: '',
						created_at: Date.now()
					});
				});

				it('should allow adding multiple rules', function* () {
					var ruleId1 = yield auth.permission.add(ID1, {
						type: 'script',
						action: ['*'],
						access: 'block',
						script: 'return 10',
						dataSchema: {}
					});
					var ruleId2 = yield auth.permission.add(ID1, {
						type: 'static',
						action: ['*'],
						resource: ID2
					});

					// ensure it exists for resource
					expect(yield auth.permission.get(ID1)).to.contain(ruleId1);
					expect(yield auth.permission.get(ID1)).to.contain(ruleId2);
				});
			});
		});

		describe('#get', function() {
			it('should return an empty array if no permissions exist', function* () {
				expect(yield auth.permission.get(ID1)).to.eql([]);
			});

			it('should get all the permissions of a resource', function* () {
				yield [
					auth.permission.add(ID1, {
						type: 'static',
						action: ['*'],
						resource: ID2
					}),
					auth.permission.add(ID1, {
						type: 'static',
						action: ['*'],
						resource: ID2
					})
				];

				var permissions = yield auth.permission.get(ID1);
				expect(permissions).to.have.length(2);
			});
		});

		describe('#getDetails', function() {
			it('should return nothing if the rule does not exist', function* () {
				expect(yield auth.permission.getDetails(ID2)).to.equal(null);
			});

			it('should return all fields', function* () {
				var ruleId = yield auth.permission.add(ID1, {
					type: 'script',
					action: ['*'],
					access: 'block',
					script: 'return 10',
					dataSchema: {}
				});

				// ensure details are correct
				var retrievedRule = yield auth.permission.getDetails(ruleId);
				eqlRule(retrievedRule, {
					_id: ruleId,
					type: 'script',
					scope: ['*'],
					action: ['*'],
					script: 'return 10',
					dataSchema: {},
					status: 'enabled',
					access: 'block',
					priority: 100,
					description: '',
					created_at: Date.now()
				});
			});
		});

		describe('#del', function() {
			it('should do nothing for non-existent rule', function* () {
				yield auth.permission.del(ID1, ID2);
			});

			it('should remove the rule', function* () {
				var ruleId = yield auth.permission.add(ID1, {
					type: 'script',
					action: ['*'],
					access: 'block',
					script: 'return 10',
					dataSchema: {}
				});

				yield auth.permission.del(ID1, ruleId);

				expect(yield auth.permission.get(ID1)).to.have.length(0);
				var retrievedRule = yield auth.permission.getDetails(ruleId);
				expect(retrievedRule).to.equal(null);

				expect(yield auth._db.keys('*')).to.have.length(0);
			});
		});
	});

	describe('#del', function() {
		it('should do nothing if the resource does not exist', function* () {
			yield auth.del(ID1);
		});

		it('should delete all keys related to the resource', function* () {
			// setup
			yield auth.parent.add(ID1, ID2);
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				access: 'block',
				script: 'return 10',
				dataSchema: {}
			});

			yield auth.del(ID1);

			// assertion
			expect(yield auth._db.keys('*')).to.have.length(0);
		});
	});

	describe('#exists', function() {
		it('should return true if resource has been assigned a parent', function* () {
			// setup
			yield auth.parent.add(ID1, ID2);

			expect(yield auth.exists(ID1)).to.equal(true);
		});

		it('should return true if resource has been assigned a permission', function* () {
			// setup
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				access: 'block',
				script: 'return 10',
				dataSchema: {}
			});

			expect(yield auth.exists(ID1)).to.equal(true);
		});

		it('should return false if resource is not in the system', function* () {
			expect(yield auth.exists(ID1)).to.equal(false);
		});

		it('should return false once a resource has been removed from the system', function* () {
			// setup
			yield auth.parent.add(ID1, ID2);
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				access: 'block',
				script: 'return 10',
				dataSchema: {}
			});

			yield auth.del(ID1);

			// assertion
			expect(yield auth.exists(ID1)).to.equal(false);
		});
	});

	describe('#hierarchy', function() {
		it('should return an empty array if resource does not exist', function* () {
			expect(yield auth.hierarchy(ID1)).to.eql({
				_id: ID1, parents: []
			});
		});

		it('should return the parents in a bottom up order', function* () {
			// multiple groups
			yield auth.parent.add(ID1, ID2);
			yield auth.parent.add(ID1, ID3);

			// grandparents ;)
			yield auth.parent.add(ID2, ID4);
			yield auth.parent.add(ID2, ID5);
			yield auth.parent.add(ID3, ID5);
			yield auth.parent.add(ID4, ID5);
			yield auth.parent.add(ID4, ID6);

			var hierarchy = yield auth.hierarchy(ID1);

			expect(hierarchy).to.have.property('_id', ID1);
			expect(hierarchy.parents).to.have.length(2);

			// i cant be stuffed so skipping this for now
			/*
			expect(hierarchy.parents).to.deep.include.members([
				{
					_id: ID2,
					parents: [
						{
							_id: ID4,
							parents: [
								{ _id: ID5, parents: [] },
								{ _id: ID6, parents: [] }
							]
						},
						{ _id: ID5, parents: [] }
					]
				},
				{
					_id: ID3,
					parents: [{ _id: ID5, parents: [] }]
				}
			]);
			*/
		});
	});

	describe('#uniqueParents', function() {
		it('should return an empty array if resource does not exist', function* () {
			expect(yield auth.uniqueParents(ID1)).to.eql([ID1]);
		});

		it('should return all parents going up the ladder', function* () {
			// multiple groups
			yield auth.parent.add(ID1, ID2);
			yield auth.parent.add(ID1, ID3);

			// grandparents ;)
			yield auth.parent.add(ID2, ID4);

			var p = yield auth.uniqueParents(ID1);
			expect(p).to.have.members([ID1, ID2, ID3, ID4]);
		});

		it('should return all parents removing duplicates', function* () {
			// multiple groups
			yield auth.parent.add(ID1, ID2);
			yield auth.parent.add(ID1, ID3);
			yield auth.parent.add(ID1, ID4);
			yield auth.parent.add(ID3, ID4);
			yield auth.parent.add(ID2, ID4);

			var p = yield auth.uniqueParents(ID1);
			expect(p).to.have.members([ID1, ID2, ID3, ID4]);
		});
	});

	// checks functionality of internal functions as well
	describe('#isAllowed', function() {
		it('should return false if requesting resource does not exist in the system', function* () {
			var called = false;

			try {
				yield auth.isAllowed(ID1, ID2, {
					action: ['*']
				});
			} catch(e) {
				called = true;
			}

			expect(called).to.equal(true);
		});

		it('should return false if requesting resource exists but no permission has been assigned', function* () {
			yield auth.permission.add(ID1, {
				type: 'static',
				action: ['*'],
				resource: ID2
			});

			expect(yield auth.isAllowed(ID1, ID3, {
				action: ['*']
			})).to.equal(false);
		});

		it('should return false if requesting resource exists but does not contain any relevant actions', function* () {
			yield auth.permission.add(ID1, {
				type: 'static',
				action: '+',
				resource: ID2
			});

			// single
			expect(yield auth.isAllowed(ID1, ID2, {
				action: '-'
			})).to.equal(false);

			// multiple
			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['-', 'x', '/']
			})).to.equal(false);
		});

		it('should return false if action matches but its not in scope', function* () {
			yield auth.permission.add(ID1, {
				type: 'static',
				scope: ['a', 'b.*'],
				action: '+',
				resource: ID2
			});

			// single
			expect(yield auth.isAllowed(ID1, ID2, {
				action: '*',
				scope: 'a.b'
			})).to.equal(false);

			// multiple
			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['+', 'x', '/'],
				scope: ['a.b', 'b']
			})).to.equal(false);
		});

		it('should return true if action and scope match', function* () {
			yield auth.permission.add(ID1, {
				type: 'static',
				scope: ['a', 'b.*'],
				action: ['+', 'x.*'],
				resource: ID2
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: '+',
				scope: ['b.a']
			})).to.equal(true);

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['+', 'x.+', 'x.-'],
				scope: ['a', 'b.a']
			})).to.equal(true);
		});

		it('should return true if static permission has been given', function* () {
			yield auth.permission.add(ID1, {
				type: 'static',
				action: ['*'],
				resource: ID2
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*']
			})).to.equal(true);
		});

		it('should return true if a parent has static permission for a resource', function* () {
			yield auth.parent.add(ID1, ID3);
			yield auth.permission.add(ID3, {
				type: 'static',
				action: ['*'],
				resource: ID2
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*']
			})).to.equal(true);
		});

		it('should return true if a parent further up the chain has static permission for a resource', function* () {
			yield auth.parent.add(ID1, ID2);
			yield auth.parent.add(ID1, ID3);
			yield auth.parent.add(ID3, ID4);
			yield auth.permission.add(ID4, {
				type: 'static',
				action: ['*'],
				resource: ID5
			});

			expect(yield auth.isAllowed(ID1, ID5, {
				action: ['*']
			})).to.equal(true);
		});

		it('should throw an error if criteria fails for a script rule', function* () {
			var called = false;

			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'return "allow"',
				dataSchema: { required: true, enum: [] }
			});

			try {
				yield auth.isAllowed(ID1, ID2, {
					action: ['*'],
					data: {}
				});
			} catch(e) {
				called = true;
			}

			expect(called).to.equal(true);
		});

		it('should throw an error if a script rule throws an error', function* () {
			var called = false;

			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'error(1)',
				dataSchema: { required: true }
			});

			try {
				yield auth.isAllowed(ID1, ID2, {
					action: ['*'],
					data: {}
				});
			} catch(e) {
				called = true;
			}

			expect(called).to.equal(true);
		});

		it('should return false if rule script returns false', function* () {
			yield auth.permission.add(ID1, {
				type: 'script',
				action: '*',
				script: 'return "block"',
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*'],
				data: {}
			})).to.equal(false);
		});

		it('should do nothing if a script returns no status', function* () {
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'return',
				dataSchema: { required: true }
			});
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'return nil',
				dataSchema: { required: true }
			});
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'return "allow"',
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*'],
				data: {}
			})).to.equal(true);
		});

		it('should return false if a deny rule overrides a parent rule', function* () {
			yield auth.parent.add(ID1, ID2);
			yield auth.permission.add(ID2, {
				type: 'static',
				action: ['*'],
				resource: ID3
			});
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'return "block"',
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*'],
				data: {}
			})).to.equal(false);
		});

		it('should return false if only a deny rule exists', function* () {
			yield auth.permission.add(ID1, {
				type: 'static',
				action: ['*'],
				access: 'block',
				resource: ID2
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*'],
				data: {}
			})).to.equal(false);
		});

		it('should return true if a script rule at a parent allows access', function* () {
			yield auth.parent.add(ID1, ID2);
			yield auth.permission.add(ID2, {
				type: 'script',
				action: ['*'],
				script: 'return "allow"',
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID3, {
				action: ['*'],
				data: {}
			})).to.equal(true);
		});

		it('should return false if a script rule at a parent denies access', function* () {
			yield auth.parent.add(ID1, ID2);
			yield auth.permission.add(ID2, {
				type: 'script',
				action: ['*'],
				script: 'return "block"',
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID3, {
				action: ['*'],
				data: {}
			})).to.equal(false);
		});

		it('should ignore script rule if it doesnt apply to resource', function* () {
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'return "allow"',
				resource: ID3,
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*'],
				data: {}
			})).to.equal(false);
		});

		it('should use script rule if it applies to resource', function* () {
			yield auth.parent.add(ID1, ID2);
			yield auth.permission.add(ID2, {
				type: 'script',
				action: ['*'],
				script: 'return "allow"',
				resource: ID5,
				dataSchema: { required: true }
			});
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'return "block"',
				resource: ID6,
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID5, {
				action: ['*'],
				data: {}
			})).to.equal(true);
		});

		it('should use script rule if it applies to parent of a resource', function* () {
			yield auth.parent.add(ID1, ID2);
			yield auth.parent.add(ID2, ID3);
			yield auth.parent.add(ID4, ID5);
			yield auth.parent.add(ID5, ID6);
			yield auth.permission.add(ID3, {
				type: 'script',
				action: ['*'],
				script: 'return "allow"',
				resource: ID6,
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID4, {
				action: ['*'],
				data: {}
			})).to.equal(true);
		});

		it('should pass correct data to user script', function* () {
			yield auth.permission.add(ID1, {
				type: 'script',
				action: ['*'],
				script: 'if data.a == "a" and data.b == "b" then return "allow" end',
				dataSchema: { required: true }
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*'],
				data: { a: 'a', b: 'b' }
			})).to.equal(true);
		});

		it('should work even if requested resource is not in the system', function* () {
			yield auth.permission.add(ID1, {
				type: 'static',
				action: ['*'],
				access: 'allow',
				resource: ID2
			});

			expect(yield auth.isAllowed(ID1, ID2, {
				action: ['*'],
				data: { a: 'a', b: 'b' }
			})).to.equal(true);
		});
	});

	//describe('#allowed', fu);
	//it('')
});