'use strict';

var _ = require('lodash');
var fs = require('fs');
var assert = require('assert');
var crypto = require('crypto');
var wildcard = require('wildcard');
var luaparser = require('luaparse');
var jsonschema = require('jsonschema');
var debug = require('debug')('authorisation');

var dbLib = require(__dirname + '/db');
var constants = require(__dirname + '/constants');

const ID_LENGTH = 16;
const SEPARATOR = constants.SEPARATOR;
const PERMISSION_TYPE = constants.PERMISSION.TYPE;
const PERMISSION_TYPES = constants.PERMISSION.TYPES;
const PERMISSION_ACCESS = constants.PERMISSION.ACCESS;
const PERMISSION_STATUS = constants.PERMISSION.STATUS;
const PARENT_RES_SUFFIX = constants.PARENT.RES_SUFFIX;
const PERMISSION_ACCESSES = constants.PERMISSION.ACCESSES;
const PERMISSION_STATUSES = constants.PERMISSION.STATUSES;
const RESOURCE_KEY_PREFIX = constants.RESOURCE.KEY_PREFIX;
const PERMISSION_KEY_PREFIX = constants.PERMISSION.KEY_PREFIX;
const PERMISSION_RES_SUFFIX = constants.PERMISSION.RES_SUFFIX;
assert(SEPARATOR && PERMISSION_TYPE && PERMISSION_STATUS && PARENT_RES_SUFFIX && RESOURCE_KEY_PREFIX && PERMISSION_KEY_PREFIX && PERMISSION_RES_SUFFIX, 'Missing constants');

var validator = new jsonschema.Validator();
var runLuaScript = fs.readFileSync(__dirname + '/run-script-at-hash-key.lua');
var defaultsDeep = _.partialRight(_.merge, function deep(value, other) {
	return _.merge(value, other, deep);
});
var baseRuleSchema = {
	required: true,
	type: 'object',
	additionalProperties: false,
	properties: {
		type: { required: true, enum: PERMISSION_TYPES },
		access: { required: false, enum: PERMISSION_ACCESSES },
		status: { required: false, enum: PERMISSION_STATUSES },
		priority: { required: false, type: 'string' },
		description: { required: false, type: 'string' },

		scope: {
			required: false,
			type: ['string', 'array'],
			items: { type: 'string' }
		},
		action: {
			required: false,
			type: ['string', 'array'],
			items: { type: 'string' }
		},
	}
};
var ruleSchema = {
	required: true,
	oneOf: [
		defaultsDeep({
			properties: {
				type: { enum: [PERMISSION_TYPE.STATIC] },
				resource: { required: true, type: 'string' }
			}
		}, baseRuleSchema),
		defaultsDeep({
			properties: {
				type: { enum: [PERMISSION_TYPE.SCRIPT] },
				script: { required: true, type: 'string' },
				dataSchema: { required: true, type: 'object' },
				// limit scope to a resource and its children
				resource: { required: false, type: 'string' }
			}
		}, baseRuleSchema)
	] 
};

function id() {
	return function(done) {
		crypto.randomBytes(ID_LENGTH / 2, function(err, buf) {
			done(err, buf && buf.toString('hex'));
		});
	};
}

module.exports = exports  = function(extensions) {
	var options = {
		db: {
			host: 'localhost',
			port: 6379,
			prefix: 'authorisation'
		}
	};
	defaultsDeep(options, extensions);

	var db = options.db.client ? options.db.client : dbLib.get(options.db);

	const KEY_PREFIX = options.db.prefix;
	assert(KEY_PREFIX, 'Missing constants');

	var singleton = { _db: db, _id: id, _ruleSchema: ruleSchema, constants: constants };

	singleton.del = function* (id) {
		var multi = db.multi();

		// remove permissions
			// cant re-use permission.del since we need to use a multi transaction here
		var permissions = yield this.permission.get(id);
		multi.del(this.permission.resourceKey(id));
		permissions.forEach(function(p) {
			multi.del(this.permission.key(p));
		}, this);

		// remove parents
		multi.del(this.parent.key(id));

		return yield multi.exec();
	};

	/*
		Returns whether a resource exists in the system
	 */
	singleton.exists = function* (id) {
		var exists = yield [
			db.exists(singleton.parent.key(id)),
			db.exists(singleton.permission.resourceKey(id))
		];
		return !!(exists[0] || exists[1]);
	};

	singleton.parent = {
		key: function(id) {
			return [KEY_PREFIX, RESOURCE_KEY_PREFIX, id, PARENT_RES_SUFFIX].join(SEPARATOR);
		},
		get: function* (id) {
			return yield db.smembers(this.key(id));
		},
		add: function* (id, parentId) {
			return yield db.sadd(this.key(id), parentId + '');
		},
		del: function* (id, parentId) {
			return yield db.srem(this.key(id), parentId + '');
		}
	};

	singleton.permission = {
		resourceKey: function(resId) {
			return [KEY_PREFIX, RESOURCE_KEY_PREFIX, resId, PERMISSION_RES_SUFFIX].join(SEPARATOR);
		},
		key: function(ruleId) {
			return [KEY_PREFIX, PERMISSION_KEY_PREFIX, ruleId].join(SEPARATOR);
		},
		get: function* (resId) {
			return yield db.zrange(this.resourceKey(resId), 0, -1);
		},
		getDetails: function* (ruleId) {
			var details = JSON.parse(yield db.get(this.key(ruleId)));

			if (details) {
				details.priority = parseInt(details.priority, 10);
				details.created_at = parseInt(details.created_at, 10);
				details.dataSchema = JSON.parse(details.dataSchema || null);
			}

			return details;
		},
		/*
			Add a permission

			@param {String} id Resource id
			@param {Object} rule Rule to add
				@param {String} [rule.description] Friendly rule description
				@param {String} rule.type Rule type - static, script
				@param {String|Array} [rule.scope=*] Rule will only apply if the same scope is mentioned in the permission check
				@param {String|Array} [rule.action=*] Action to allow. By default, all actions are allowed
				@param {String} [rule.access=allow] Whether the rule allows or blocks access
				@param {String} [rule.status=enabled] Rule status
				@param {String} [rule.resource] Static resource to allow permissions for
				@param {String} [rule.script] Lua script to run on the resource to assert whether it works
				@param {Object} [rule.dataSchema] If script rule, provide a schema used to validate data
				@param {Number} [rule.priority=100] Priority in which rules are executed
		 */
		add: function* (resId, rule) {
			var validationResult = validator.validate(rule, ruleSchema);
			assert(validationResult.valid, validationResult.errors);

			// defaults
			_.defaults(rule, {
				_id: (yield id()),
				scope: ['*'],
				action: ['*'],
				access: PERMISSION_ACCESS.ALLOW,
				status: PERMISSION_STATUS.ENABLED,
				priority: 100,
				description: '',
				created_at: Date.now()
			});

			if (!Array.isArray(rule.scope)) {
				rule.scope = [rule.scope];
			}

			if (!Array.isArray(rule.action)) {
				rule.action = [rule.action];
			}

			// validate lua script coz we're nice ;)
			if (rule.script) {
				try {
					luaparser.parse(rule.script);
				} catch(e) {
					e.message = 'Error in lua script - ' + e.message;
					throw e;
				}
			}

			if (rule.dataSchema) {
				rule.dataSchema = JSON.stringify(rule.dataSchema);
			}

			var score = parseInt('' + rule.priority + rule.created_at, 10);

			// atomic save
			var multi = db.multi()
				// using created_at and priority helps maintain natural order as well as explicit priority based order
				.zadd(this.resourceKey(resId), score, rule._id)
				.set(this.key(rule._id), JSON.stringify(rule));
			yield multi.exec();

			// return rule id
			return rule._id;
		},

		del: function* (id, ruleId) {
			var multi = db.multi()
				.zrem(this.resourceKey(id), ruleId)
				.del(this.key(ruleId));

			return yield multi.exec();
		}
	};

	singleton.hierarchy = function* (id) {
		var hierarchy = {
			_id: id,
			parents: yield this.parent.get(id)
		};
		// recursive
		hierarchy.parents = yield hierarchy.parents.map(this.hierarchy, this);

		return hierarchy;
	};

	singleton._uniqueParents = function* (id) {
		var parents = yield this.parent.get(id);

		parents.push.apply(parents, yield parents.map(this.uniqueParents, this));

		return parents;
	};

	singleton.uniqueParents = function* (id) {
		var parents = yield this._uniqueParents(id);

		parents.unshift(id);

		return _.uniq(_.flatten(parents));
	};

	singleton._matchOne = function(required, availableArr) {
		return !availableArr.every(function(available) {
			return !wildcard(required, available);
		});
	};

	singleton._match = function(requiredArr, availableArr) {
		// every one of the required values need to match
		return requiredArr.every(function(required) {
			// a required value must match one of the available values
			return availableArr.some(function(available) {
				return wildcard(available, required);
			});
		});
	};

	singleton._checkPermission = function* (permissionId, resourceParents, opt) {
		debug('_checkPermission -> Passed arguments: %j', arguments);

		var permission = yield this.permission.getDetails(permissionId);
		debug('_checkPermission -> Fetched rule: %j', permission);

		// break if rule is disabled
		if (PERMISSION_STATUS.DISABLED === permission.status) { return; }

		// break if rule is not in scope
		if (!this._match(opt.scope, permission.scope)) { return; }
		debug('_checkPermission -> Passed scope check');

		// break if action doesnt match
		if (!this._match(opt.action, permission.action)) { return; }
		debug('_checkPermission -> Passed action check');

		// ensure it applies to resource
		if (permission.resource && resourceParents.indexOf(permission.resource) < 0) { return; }
		debug('_checkPermission -> Passed resource check');

		debug('_checkPermission -> Passed all pre-conditions');

		if (PERMISSION_TYPE.STATIC === permission.type) {
			return permission.access;
		} else {
			var validationResult = validator.validate(opt.data, permission.dataSchema);
			assert(validationResult.valid, 'Data failed to pass rule data schema. Errors: ' + validationResult.errors);

			return yield db.eval(runLuaScript, 1, this.permission.key(permissionId), 'script', JSON.stringify(opt.data)); // jshint ignore:line
		}
	};

	singleton._checkPermissions = function* (requester, resourceParents, opt) {
		var access;
		var permissions = yield this.permission.get(requester);

		for (let i = 0, len = permissions.length; i < len; i += 1) {
			// returns nothing if it has nothing to do with the resource
				// so we break if a status was returned
			access = yield this._checkPermission(permissions[i], resourceParents, opt);

			assert(
				[PERMISSION_ACCESS.ALLOW, PERMISSION_ACCESS.BLOCK, void 0, null].indexOf(access) > -1,
				'Invalid status returned by rule: ' + permissions[i]
			);

			if (access) { break; }
		}

		return access;
	};

	singleton._access = function* (requesterHierarchy, resourceParents, opt) {
		var status = yield this._checkPermissions(requesterHierarchy._id, resourceParents, opt);
		var parents = requesterHierarchy.parents;

		if (!status) {
			debug('_isAllowed -> No relevant rules found for: ' + requesterHierarchy._id);

			for (let i = 0, len = parents.length; i < len; i += 1) {
				status = yield this._checkPermissions(parents[i], resourceParents, opt);
				if (status) { break; }
			}
		}

		if (!status) {
			for (let i = 0, len = parents.length; i < len; i += 1) {
				status = yield this._access(parents[i], resourceParents, opt);
				if (status) { break; }
			}
		}

		return status;
	};

	/*
		Returns if a resource has permission for another resource

		@param {String} requester Resource requesting access
		@param {String} resource Restricted resource
		@param {Object} [opt] Options
			@param {String|Array} [opt.scope] Scope for which the access is requested
			@param {String|Array} [opt.action] Action to check permission for
			@param {Any} [opt.data] Data passed to dynamic rules
		}
	*/
	singleton.isAllowed = function* (requester, resource, opt) {
		opt = opt || {};
		_.defaults(opt, { action: ['*'], scope: ['*'], data: {} });

		if (!Array.isArray(opt.action)) {
			opt.action = [opt.action];
		}
		if (!Array.isArray(opt.scope)) {
			opt.scope = [opt.scope];
		}

		debug('isAllowed -> args - requester: %s, resource: %s, opt: %j', requester, resource, opt);

		// short circuit if the requester does not exist
			// we dont need to check if the resource exists since it may simply exist as a resource property on permission rules
		var exists = yield singleton.exists(requester);
		assert(exists, 'Resource ' + requester + ' is not recognised');

		var res = yield [this.hierarchy(requester), this.uniqueParents(resource)];
		debug('isAllowed -> requester hierarchy and resource parents: %j', res);

		var access = yield this._access(res[0], res[1], opt);
		return (PERMISSION_ACCESS.ALLOW === access ? true : false);
	};

	return singleton;
};