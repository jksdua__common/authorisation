'use strict';

var redis = require('redis');
var wrap = require('co-redis');

exports.get = function(options) {
	// tmp - only required for redis 0.12.x and node 0.11.x
	var net = require('net');
	var oldConnect = net.connect;
	net.connect = net.createConnection = function(opt) {
		try {
			return oldConnect.call(net, opt);
		} catch(e) {
			opt.family = opt.family.replace('IPv', '');
			return oldConnect.call(net, opt);
		}
	};

	var db = wrap(redis.createClient(options.port, options.host, options));

	// cleanup to original state
	net.connect = net.createConnection = oldConnect;

	process.once('exit', db.quit.bind(db));

	return db;
};