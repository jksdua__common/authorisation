'use strict';

var _ = require('lodash');

// db key namespace separator
exports.SEPARATOR = '::';

exports.RESOURCE = {
	KEY_PREFIX: 'resource'
};

// parent constants
exports.PARENT = {
	RES_SUFFIX: 'parents'
};

// permission constants
var PERMISSION = exports.PERMISSION = {
	TYPE: { STATIC: 'static', SCRIPT: 'script' },
	ACCESS: { BLOCK: 'block', ALLOW: 'allow' },
	STATUS: { ENABLED: 'enabled', DISABLED: 'disabled' },
	RES_SUFFIX: 'permissions',
	KEY_PREFIX: 'permission'
};
PERMISSION.TYPES = _.values(PERMISSION.TYPE);
PERMISSION.ACCESSES = _.values(PERMISSION.ACCESS);
PERMISSION.STATUSES = _.values(PERMISSION.STATUS);