-- gets the script string from the db
	-- unfortunately, this isn't cached so needs to be loaded every time
	-- we are assuming it is relatively small and the performance is negligible

-- example permission script
	-- return data.age >= 18

-- function to load script for both lua 5.1 and 5.2
local function load_code(code, environment)
	if setfenv and loadstring then
		local f = assert(loadstring(code))
		setfenv(f, environment)
		return f
	else
		return assert(load(code, nil, 't', environment))
	end
end

-- get script from db and unpack json data
local rule = redis.call('get', KEYS[1])
local script = cjson.decode(rule).script

-- create context with access to data and globals
local context = { data = cjson.decode(ARGV[2]) }
setmetatable(context, { __index = _G })

redis.log(redis.LOG_DEBUG, 'Running script located at ' .. KEYS[1] .. '.' .. ARGV[1] .. 'with data: ' .. ARGV[2])

-- unpacks the json data before passing to the script
return load_code(script, context)()