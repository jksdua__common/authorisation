Authorisation
=============

Generic authorisation framework


Installation
------------

```bash
$ npm install --save git+ssh://git@gitlab.com:jksdua__common/auhtorisation.git#v0.0.2
```


Usage
-----

```js
var authorisation = require('js_authorisation')({
	host: 'localhost', port: 6379
});

// returns the unique rule identifier
yield authorisation.permission.add('1', {
	type: 'static',
	description: 'Sample permission rule',
	action: '*',
	resource: '2'
});

// returns boolean
yield authorisation.isAllowed('1', '2', {
	action: '*',
	scope: ['general', 'account']
});
```

### Options

The default options are shown below:

```js
{
	db: {
		host: 'localhost',
		port: 6379,
		prefix: 'authoorisation'
	}
}
```

Otherwise, a redis client instance may also be passed:

```js
{
	db: {
		client: require('redis').createClient()
	}
}
```

Methods
-------

### permission

#### permission.add(id, rule)

Adds a new permission for a resource.

```js

```

### parent

Namespace for managing resource hierarchies / groups

#### parent.add(id, parentId)

Adds a new direct parent for a resource

```js
yield authorisation.parent.add('1', '2');
```

#### parent.get(id)

Returns all direct parents of a resource

```js
yield authorisation.parent.add('1', '2');
```

#### parent.del(id, parentId)

Removes a direct parent of a resource

```js
yield authorisation.parent.del('1', '2');
```

### exists(id)

Returns whether a resource exists in the authorisation database.

```js
// true/false
yield authorisation.exists('1');
```

### del(id)

Deletes a resource from the database. All relationships are automatically removed.

```js
yield authorisation.del('1');
```


Changelog
---------

### v0.0.2 (16 Oct 2014)
- Added support for scope as an additional query parameter for permission rules
- Added support for wildcard actions and scope
- Added support for multiple actions and scopes on permission rules (adding/querying)

### v0.0.1
- Initial commit