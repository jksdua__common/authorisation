/*
	REPL for interacting with the module
		- useful for debugging
 */

var repl = require('repl').start('$ ');

repl.context.auth = require(__dirname + '/../src')();